import React from "react";
import ItemsContainer from "../ItemsContainer/ItemsContainer";
import styles from "./Main.module.scss";

const Main = (props) => {
  const { items, addToCart, openModal, closeModal, isOpenFirst, setFavorite } = props;
  return (
    <div className={styles.container}>
      <ItemsContainer
        items={items}
        addToCart={addToCart}
        openModal={openModal}
        closeModal={closeModal}
        isOpenFirst={isOpenFirst}
        setFavorite={setFavorite}
      />
    </div>
  );
};

export default Main;
