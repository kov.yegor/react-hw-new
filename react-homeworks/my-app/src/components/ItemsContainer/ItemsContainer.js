import React from "react";
import styles from "./ItemsContainer.module.scss";
import Item from "../Item/Item";

const ItemsContainer = (props) => {
  const { items, addToCart, setFavorite } = props;

  return (
    <section className={styles.root}>
      <div className={styles.container}>
        {items &&
          items.map((item) => (
            <Item
              key={item.article}
              {...item}
              addToCart={addToCart}
              setFavorite={setFavorite}
            />
          ))}
      </div>
    </section>
  );
};

export default ItemsContainer;
