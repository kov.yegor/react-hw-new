import React from "react";
import styles from "./Item.module.scss";
import Button from "../Button/Button";
import { ReactComponent as StarIcon } from "../../assets/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/star-remove.svg";

const   Item = (props) => {
  const { name, price, article, url, addToCart, isFavorite, setFavorite } = props;


  return (
    <div className={styles.root}>
      <div className={styles.favourites} onClick={() => setFavorite(name, price, article, url,)}>
        {isFavorite ? <StarRemove /> : <StarIcon />}
      </div>
      <p>{name}</p>
      <span>{price}</span>
      <img src={url} alt={name} />
     
      <Button handleClick={() => addToCart(name, price, article, url)} text="Add to cart" />
    </div>
  );
};

export default Item;
