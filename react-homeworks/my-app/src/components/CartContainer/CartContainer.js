import React from 'react';
import Item from '../Item/Item';
import styles from './CartContainer.module.scss'
import {getItemFromLS} from '../../utils/localStorage'
import CartItem from '../../components/CartItem/CartItem'

const CartContainer = (props) => {
    const {removeFromCart, setCartItem} = props
    
    const cartData = getItemFromLS('cartItem')

  return (
    <section className={styles.root}>
     
        <div className={styles.container}>
        {cartData && cartData.map((elem)=><CartItem setCartItem={setCartItem} count={elem.count} title={elem.name} id={elem.id} img={elem.url} key={elem.id} removeFromCart={removeFromCart} />)}
        </div>
    </section>
)



}

export default CartContainer;