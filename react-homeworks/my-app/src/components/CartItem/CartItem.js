import React from 'react';
import styles from './CartItem.module.scss';
import Button from '../Button/Button'

const CartItem = (props) => {
    const {count,  article, img,text, setCartItem} = props;

    const removeFromCart = (article) => {

        setCartItem((state) => {
        


            const cartData = localStorage.getItem('cartItem')
          const index = state.findIndex((item) => item.article === article);
          const newCartItem = state;
          let splicedArr = newCartItem.splice(index, 1);
          const jsonCartItem = JSON.stringify(splicedArr);
          localStorage.setItem("cartItem", jsonCartItem);
          return newCartItem;
        });
      };
    return (
        <>

        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src={img} alt={text}/>
                </div>
                <span className={styles.title}>{text}</span>
            </div>


            <span className={styles.quantity}>{count}</span>

            <div className={styles.btnContainer}>
       
                <Button text ="Delete" handleClick={()=>removeFromCart(article)}></Button>
            </div>

        </div>
            </>
    )
    }

export default CartItem;