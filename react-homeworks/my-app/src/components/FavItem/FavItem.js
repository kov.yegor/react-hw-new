import React from 'react';
import styles from './FavItem.module.scss';
import Button from '../Button/Button'

const FavItem = (props) => {
    const {price, text, img, deleteFromFav} = props

    return (
        <>

        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src={img} alt={text}/>
                </div>
                <span className={styles.title}>{text}</span>
                <span className={styles.title}>{price}</span>
                <Button text='Delete' handleClick={deleteFromFav}/>
            </div>
        </div>
            </>
    )
}

export default FavItem;