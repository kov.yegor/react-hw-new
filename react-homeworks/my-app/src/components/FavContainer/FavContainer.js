import React from 'react';
import styles from './FavContainer.module.scss'
import {getItemFromLS} from '../../utils/localStorage'
import FavItem from '../FavItem/FavItem'

const FavContainer = (props) => {
    const {setFavorite, deleteFromFav} = props;
const favData = getItemFromLS('favItems')
    console.log(favData);
    return (
        <section className={styles.root}>
         
            <div className={styles.container}>
            {!favData ? <p>Add some t-shirts to favourites</p>
             : favData.map((elem)=>
             <FavItem count={elem.count} title={elem.name} id={elem.id} img={elem.url} key={elem.id} setFavorite={setFavorite} deleteFromFav={deleteFromFav} />)}
            </div>
        </section>
    )
}

export default FavContainer;