import React from 'react'
import styles from './Modal.module.scss'
import Button from '../Button/Button'

class Modal extends React.Component {





render() {
const {closeModal, header, text, closeButton, handleClick, name, price} = this.props

    return (

        <div className={styles.modal} onClick={closeModal}>
                   
                    <div className={styles.content}>
                        {closeButton &&
                        <button className={styles.closeBtn} onClick={closeModal}>X</button>}
                        <h1>{header}</h1>
                        <p>{text}</p>
                        <Button handleClick={handleClick}  title={'Ok'}/>
                        <Button handleClick={closeModal} title={'Cancel'}/>
                    </div>
                </div>
                
    )
}
}

export default Modal;