import React from "react";
import Header from "./components/Header/Header";
import AppRoutes from "./Routes/Routes";
import { useEffect, useState } from "react";


const App = () => { 
  const [closeButton, setCloseButton] = useState(true);
  const [items, setItems] = useState([]);
  const [cartItem, setCartItem] = useState([]);
  const [favItems, setFavItems] = useState([]);

  const fetchItems = async () => {
    const data = await fetch("./store.json").then((data) => data.json());
    setItems(data);
  };
  useEffect(() => {
    fetchItems();
  }, []);

  const addToCart = (name, price, article, url) => {
    setCartItem((state) => {
      const index = state.findIndex((item) => item.article === article);
      if (index === -1) {
        const newCartItem = {
          state: [...state, { name, price, article, url, count: 1 }],
        };
        const jsonCartItem = JSON.stringify(newCartItem.state);
        if (jsonCartItem) {
          localStorage.setItem("cartItem", jsonCartItem);
        }
        return newCartItem.state;
      } else {
        const newCartItem = state;
        newCartItem[index].count += 1;
        const jsonCartItem = JSON.stringify(newCartItem);
        localStorage.setItem("cartItem", jsonCartItem);
        return cartItem;
      }
    });
  };
  // const removeFromCart =()=>{
  //   setCartItem((cartItem)=>{
  //     const favItemsLS = localStorage.getItem("favItems");
  //   if (favItemsLS) {
  //     localStorage.removeItem(`favItems`);
  //   }
  //   const newCartItem = cartItem;
  //   return newCartItem.cartItem;
  //   })
  // }

  const addToFavorite = (name, price, article, url) => {
    setFavItems((favItems) => {
      const index = favItems.findIndex((items) => items.article === article);
      if (index === -1) {
        const newFavItem = {
          favItems: [...favItems, { name, price, article, url, isFavorite: true }],
        };
        const jsonFavItem = JSON.stringify(newFavItem.favItems);
        if (jsonFavItem) {
          localStorage.setItem("favItems", jsonFavItem);
        }
        return newFavItem.favItems;
      } else {
        const favItemsLS = localStorage.getItem("favItems");
        if (favItemsLS) {
          localStorage.removeItem(`favItems`);
        }
        const newFavItem = favItems;
        return newFavItem.favItems;
      }
    });
  };
 const deleteFromFav = () => {
   setFavItems((favItems)=>{
    const favItemsLS = localStorage.getItem("favItems");
    if (favItemsLS) {
      localStorage.removeItem(`favItems`);
    }
    const newFavItem = favItems;
    return newFavItem.favItems;
   })

}
 

  return (
    <>
      <Header />
      <AppRoutes
      cartItem={cartItem}
      // favItems={ favItems}
        items={items}
        addToCart={addToCart}
        setFavorite={addToFavorite}
     deleteFromFav={deleteFromFav}
    //  removeFromCart={removeFromCart}
        setCartItem={setCartItem}
      />
      
    </>
  );
};
export default App;
