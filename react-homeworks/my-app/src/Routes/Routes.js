import React from 'react';

import {Routes, Route} from "react-router-dom";
import HomePage from '../pages/HomePage/HomePage';
import CartPage from '../pages/CartPage/CartPage';
import FavPage from '../pages/FavPage/FavPage'

const AppRoutes = (props) => {
    const {  items, addToCart,setFavorite, removeFromCart, cartItem, setCartItem, deleteFromFav} = props
    return (
        <Routes>
            <Route path='/' element={<HomePage  items={items} addToCart={addToCart} setFavorite={setFavorite}/>}/>
            <Route path='/cart' element={<CartPage cartItem={cartItem} removeFromCart={removeFromCart} setCartItem={setCartItem} />}/>
            <Route path='/fav' element={<FavPage setFavorite={setFavorite} deleteFromFav={deleteFromFav} />}/>
        </Routes>
    )
}
export default AppRoutes;