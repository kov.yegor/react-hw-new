import React, {useEffect} from 'react';
import Main from '../../components/Main/Main'

const HomePage = (props) => {
const {  items, addToCart,setFavorite} = props

    return (
        <>
 <Main items={items} addToCart={addToCart} setFavorite={setFavorite} />
        </>
    )
}
export default HomePage;