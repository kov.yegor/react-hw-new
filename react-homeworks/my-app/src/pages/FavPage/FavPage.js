import React from 'react';
import FavContainer from '../../components/FavContainer/FavContainer'
const FavPage = (props) => {
const {setFavorite, deleteFromFav} = props;
    return (
        <>
<FavContainer setFavorite={setFavorite} deleteFromFav={deleteFromFav} />
        </>
    )
}
export default FavPage;