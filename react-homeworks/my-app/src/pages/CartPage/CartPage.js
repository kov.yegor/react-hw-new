import React from 'react';
import CartContainer from '../../components/CartContainer/CartContainer'
const CartPage = (props) => {
const {removeFromCart, setCartItem, cartItem} = props;
    return (
        <>
<CartContainer cartItem={cartItem} removeFromCart={removeFromCart} setCartItem={setCartItem} />
        </>
    )
}
export default CartPage;